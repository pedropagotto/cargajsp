/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.cargafacil.objetos;

/**
 *
 * @author pepag
 */
public class Usuario {
    
    private String idusu;
    private String nomefantasia;
    private String razaosocial;
    private String cidade;
    private String uf;
    private String senha;
    private String email;
    private String confsenha;
    private Integer cel;
    private Integer cnpj;
    private Integer telefone;
    private Integer ie;
    private Integer cep;
    
   public Usuario(){
       

}

    /**
     * @return the nomefantasia
     */
    public String getNomefantasia() {
        return nomefantasia;
    }

    /**
     * @param nomefantasia the nomefantasia to set
     */
    public void setNomefantasia(String nomefantasia) {
        this.nomefantasia = nomefantasia;
    }

    /**
     * @return the razaosocial
     */
    public String getRazaosocial() {
        return razaosocial;
    }

    /**
     * @param razaosocial the razaosocial to set
     */
    public void setRazaosocial(String razaosocial) {
        this.razaosocial = razaosocial;
    }

    /**
     * @return the cidade
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * @param cidade the cidade to set
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * @return the uf
     */
    public String getUf() {
        return uf;
    }

    /**
     * @param uf the uf to set
     */
    public void setUf(String uf) {
        this.uf = uf;
    }

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the confsenha
     */
    public String getConfsenha() {
        return confsenha;
    }

    /**
     * @param confsenha the confsenha to set
     */
    public void setConfsenha(String confsenha) {
        this.confsenha = confsenha;
    }

    /**
     * @return the cel
     */
    public Integer getCel() {
        return cel;
    }

    /**
     * @param cel the cel to set
     */
    public void setCel(Integer cel) {
        this.cel = cel;
    }

    /**
     * @return the cnpj
     */
    public Integer getCnpj() {
        return cnpj;
    }

    /**
     * @param cnpj the cnpj to set
     */
    public void setCnpj(Integer cnpj) {
        this.cnpj = cnpj;
    }

    /**
     * @return the telefone
     */
    public Integer getTelefone() {
        return telefone;
    }

    /**
     * @param telefone the telefone to set
     */
    public void setTelefone(Integer telefone) {
        this.telefone = telefone;
    }

    /**
     * @return the ie
     */
    public Integer getIe() {
        return ie;
    }

    /**
     * @param ie the ie to set
     */
    public void setIe(Integer ie) {
        this.ie = ie;
    }

    /**
     * @return the cep
     */
    public Integer getCep() {
        return cep;
    }

    /**
     * @param cep the cep to set
     */
    public void setCep(Integer cep) {
        this.cep = cep;
    }

    /**
     * @return the idusu
     */
    public String getIdusu() {
        return idusu;
    }

    /**
     * @param idusu the idusu to set
     */
    public void setIdusu(String idusu) {
        this.idusu = idusu;
    }

    
    
    
    
}
