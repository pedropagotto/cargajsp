<%-- 
    Document   : slid
    Created on : 05/09/2015, 18:29:04
    Author     : lucas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head> 
        <link rel="stylesheet" type="text/css" href="slid_jQuery/slid.css"/>  
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="slid_jQuery/js/jquery.js"></script>
        <script type="text/javascript" src="slid_jQuery/js/funcoes.js"></script>
        <link rel="icon" href="css/icones/carro.ico" sizes="24x24"/>
        <title>JSP Page</title>
    </head>
    <body>
        
         <!--- INICIANDO O SLID  BY: BLINDÃO! :B  --->
         
                
                                    
                    <!--- • chamando os botoes next/prev --->
                    <section id="galeria">
                      <section id="buttons">
                        <a href="#" class="prev">&laquo;</a>
                        <a href="#" class="next">&raquo;</a>                      
                    </section><!--- finalizando botoes next/prev --->
                    
                    
                        <!--- 1º slid --->
                        <ul>
                            
                            
                        <li>
                            <span> Teste img 1
                            </span>  
                            <img src="slid_jQuery/imagens/Dark-Space-Wallpaper.jpg">
                            
                        </li>                         
                      
                      
                         <!--- 2º slid --->
                       
                        <li>
                            <span> Teste IMG 2
                            </span>                            
                            <img src="slid_jQuery/imagens/skull_manipulation_wallpaper_by_ohmybrooke-d76sgh7.jpg">
                        </li>                      
                         
                          <!--- 3º slid --->

                        <li>
                            <span> Teste IMG3
                            </span>                            
                            <img src="slid_jQuery/imagens/volvo_fh_1.jpg">
                        </li> 
                        
                        
                        
                         </ul>                        
                    </section>
        
        
        
        
        
        
        
        
    </body>
</html>
